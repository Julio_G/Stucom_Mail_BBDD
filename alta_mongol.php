<html>
    <head>
        <title> ALTA EN BASE DE DATOS </title>
    </head>
    <body>
        <?php
        $nombre = trim($_POST['nomalta']);
        $ape = trim($_POST['apealta']);
        $curso = trim($_POST['curalta']);
        $conn = new MongoClient() or die("Error al conectar con la base de datos");
        $db = $conn->selectDB('DAW!');
        $coll = $db->selectCollection('alumnos');
        $post = array(
            'nombre' => $nombre,
            'apellido' => $ape,
            'curso' => $curso);
        $coll->insert($post);
        print "<h2>Datos introducidos </h2><br>";
        $conn->close();
        ?>
    </body>
</html>