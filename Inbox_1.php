<?php

session_start();
// Nos aseguramos de que haya un usuario autentificado
if (isset($_SESSION["username"])) {
    require_once 'bbdduser.php';
    $username = $_SESSION["username"];

    if (isset($_POST['VerMessage'])) {
        $idmessage = $_POST['idmessage'];
        changeread($idmessage);
        $vermensaje = selectByIdMessage($idmessage);

        echo "<h1>Ver Mensajes</h1><br>";
        echo "<table>";
        echo "<tr>";
        echo "<th>EMISOR</th>";
        echo "<th>FECHA/HORA</th>";
        echo "<th>LEIDO</th>";
        echo "<th>ASUNTO</th>";
        echo "<th>CUERPO</th>";
        echo "</tr>";

        while ($fila = mysqli_fetch_array($vermensaje)) {
            extract($fila);
            echo "<tr>";
            echo "<td>$sender</td>";
            echo "<td>$date</td>";
            if ($read == 1) {
                echo "<td>Si</td>";
            } else {
                echo"<td>No</td>";
            }
            echo "<td>$subject</td>";
            echo "<td>$body</td>";
        }
        echo "</table>";
        echo "<p><a href='Inbox.php'>Volver a Bandeja Entrada</a></p>";
    } else {
        insertEventListMessage($username);
        if (isset($_GET["posicion"])) {
            $posicion = $_GET["posicion"];
        } else {
            $posicion = 0;
        }
        $listamensajes = selectMessagesByUser($username, $posicion, 10);
        $total = totalMessages($username);
        echo "<h1>Listado Mensajes Recibidos</h1><br>";
        echo "<table>";
        echo "<tr>";
        echo "<th>EMISOR</th>";
        echo "<th>FECHA/HORA</th>";
        echo "<th>ASUNTO</th>";
        echo "<th>LEIDO</th>";
        echo "</tr>";

        while ($fila = mysqli_fetch_array($listamensajes)) {
            extract($fila);
            echo "<form action ='' method = 'POST'>";
            echo "<tr>";
            echo "<td>$sender</td>";
            echo "<td>$date</td>";
            echo "<td>$subject</td>";
            if ($read == 1) {
                echo "<td>Si</td>";
            } else {
                echo"<td>No</td>";
            }
            echo "<td><input type='hidden' value=$idmessage name='idmessage'></td>";
            echo "<td><input type='submit' value='Leer' name='VerMessage'>";
            echo "</tr>";
            echo "</form>";
        }
        echo "</table>";
        if ($posicion > 0) {
            echo "<a href='Inbox.php?posicion=" . ($posicion - 10) . "'>&lt;&lt;</a>";
        }
        if ($posicion + 10 <= $total) {
            echo "Mostrando " . ($posicion + 1) . " al " . ($posicion + 10) . " de $total ";
        } else {
            echo "Mostrando " . ($posicion + 1) . " al $total de $total";
        }
        if ($posicion + 10 < $total) {
            echo "<a href='Inbox.php?posicion=" . ($posicion + 10) . "'>&gt;&gt;</a>";
        }
        echo "<p><a href='UserHome.php'>Volver</a></p>";
    }
} else {
    echo "No estás autentificado.";
    echo "<p><a href='Index.php'>Volver</a></p>";
}
 