<?php

session_start();
// Nos aseguramos de que haya un usuario autentificado
if (isset($_SESSION["username"])) {
    require_once 'bbdduser.php';

    $username = $_SESSION["username"];

    insertEventListMessage($username);
    $listamensajes = selectMessegesByUser($username);

    echo "<h1>Listado Mensajes</h1><br>";
    echo "<table>";
    echo "<tr>";
    echo "<th>EMISOR</th>";
    echo "<th>FECHA/HORA</th>";
    echo "<th>ASUNTO</th>";
    echo "<th>LEIDO</th>";
    echo "</tr>";

    while ($fila = mysqli_fetch_array($listamensajes)) {
        extract($fila);
        echo "<form action='' method='POST'>";
        echo "<tr>";
        echo "<td>$sender</td>";
        echo "<td>$date</td>";
        echo "<td>$subject</td>";
        echo "<td>$read</td>";
        echo "<td><input type='hidden' value='$idmessage' name='eventmessage'></td>";
        echo "<td><input type='submit' value='Leer' name='VerMessages'>";
        echo "</tr>";
    }
    echo "</table>";
} else {
    echo "No estás autentificado.";
}
    