<?php

require_once 'bbdd.php';

function cambiarPass($username, $pass) {
    $con = conectar("msg");
    $update = "update user set password='$pass' where username='$username'";
    if (mysqli_query($con, $update)) {
        echo "<p>Contraseña Modificada Correctamente</p>";
    } else {
        echo mysqli_error($con);
    }
    desconectar($con);
}

function updateUser($username, $pass, $email) {
    $con = conectar("msg");
    $update = "update user set password='$pass', email='$email' where username='$username'";
    if (mysqli_query($con, $update)) {
        echo "<p>Datos del perfil modificiados</p>";
    } else {
        echo mysqli_error($con);
    }
    desconectar($con);
}

function getUser($username) {
    $con = conectar("msg");
    $query = "select * from user where username='$username'";
    $resultado = mysqli_query($con, $query);
    desconectar($con);
    return $resultado;
}

function getTipoUsuario($username) {
    $con = conectar("msg");
    $query = "select type from user where username='$username'";
    $resultado = mysqli_query($con, $query);
    $fila = mysqli_fetch_array($resultado);
    extract($fila);
    desconectar($con);
    return $type;
}

function verificarUser($username, $pass) {
    $con = conectar("msg");
    $query = "select password from user where username='$username'";
    $resultado = mysqli_query($con, $query);
    $filas = mysqli_num_rows($resultado);
    desconectar($con);
    if ($filas > 0) {
        // Comprobamos que la contraseña es correcta
        $fila = mysqli_fetch_array($resultado);
        extract($fila);
        return password_verify($pass, $password);
    } else {
        return false;
    }
}

function insertUser($nusuario, $passcif, $nombre, $apellido, $tipo) {
    $conexion = conectar("msg");
    $insert = "insert into user values "
            . "('$nusuario', '$passcif', '$nombre', '$apellido', $tipo)";
    if (mysqli_query($conexion, $insert)) {
        echo "<p>Usuario dado de alta</p>";
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}

function existeUsuario($nombre_usuario) {
    $conexion = conectar("msg");
    $consulta = "select * from user where username='$nombre_usuario';";
    $resultado = mysqli_query($conexion, $consulta);
    $contador = mysqli_num_rows($resultado);
    desconectar($conexion);
    if ($contador == 0) {
        return false;
    } else {
        return true;
    }
}

function selectAllUsers() {
    $con = conectar('msg');
    $select = "select * from user";
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}

function borrarUser($cp) {
    $con = conectar("msg");
    $delete = "delete from user where username= '$cp'";
    if (mysqli_query($con, $delete)) {
        echo "Usuario Borrado";
    } else {
        echo mysqli_error($con);
    }
    desconectar($con);
}

function insertEvent($username) {
    $conexion = conectar("msg");
    $insert = "insert into event values "
            . "('', '$username', (select current_timestamp), 'I')";
    if (mysqli_query($conexion, $insert)) {
        
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}

function insertEventListMessage($username) {
    $conexion = conectar("msg");
    $insert = "insert into event values "
            . "('', '$username', (select current_timestamp), 'C')";
    if (mysqli_query($conexion, $insert)) {
        
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}

function selectMessegesByUser($username) {
    $con = conectar('msg');
    $select = "select * from message where receiver= '$username' order by date desc";
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}
