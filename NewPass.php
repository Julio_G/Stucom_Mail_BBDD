<!DOCTYPE html>
<!--  Formulario para modificar perfil del usuario -->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar perfil</title>
    </head>
    <body>
        <?php
        require_once 'bbdduser.php';
        session_start();

        if (isset($_SESSION["username"])) {
            $username = $_SESSION["username"];

            if (isset($_POST["comprobar"])) {
                $pass = $_POST["pass"];
                $verfipass = $_POST["verfipass"];
                if ($verfipass == $pass) {
                    $passcif = password_hash($pass, PASSWORD_DEFAULT);
                    cambiarPass($username, $passcif);
                    echo "<p><a href = 'UserHome.php'>Volver</a></p>";
                } else {
                    echo "Contraseñas no coinciden";
                    echo "<p><a href = 'NewPass.php'>Cambiar Contraseña</a></p>";
                }
            } else {
                // Traemos los datos actuales del usuario
                echo "<form action='' method='POST'>";
                echo "<h2>Perfil de $username</h2>";
                echo "<p>Nueva Password: <input type='password' name='pass' required></p>";
                echo "<p>Verificar Password: <input type='password' name='verfipass' required></p>";
                echo "<p><input type='submit' name='comprobar' value='Modificar'></p>";
                echo "</form>";
                echo "<p><a href = 'UserHome.php'>Volver</a></p>";
            }
        } else {
            echo "Usuario no autentificado";
        }
        ?>
    </body>
</html>
