<?php

require_once 'bbdd.php';

function cambiarPass($username, $pass) {
    $con = conectar("msg");
    $update = "update user set password='$pass' where username='$username'";
    if (mysqli_query($con, $update)) {
        echo "<p>Contraseña Modificada Correctamente</p>";
    } else {
        echo mysqli_error($con);
    }
    desconectar($con);
}

function updateUser($username, $pass, $email) {
    $con = conectar("msg");
    $update = "update user set password='$pass', email='$email' where username='$username'";
    if (mysqli_query($con, $update)) {
        echo "<p>Datos del perfil modificiados</p>";
    } else {
        echo mysqli_error($con);
    }
    desconectar($con);
}

function getUser($username) {
    $con = conectar("msg");
    $query = "select * from user where username='$username'";
    $resultado = mysqli_query($con, $query);
    desconectar($con);
    return $resultado;
}

function getTipoUsuario($username) {
    $con = conectar("msg");
    $query = "select type from user where username='$username'";
    $resultado = mysqli_query($con, $query);
    // Extraemos el tipo para devolver ya el string con el tipo de user
    $fila = mysqli_fetch_array($resultado);
    extract($fila);
    desconectar($con);
    return $type;
}

function verificarUser($username, $pass) {
    $con = conectar("msg");
    $query = "select password from user where username='$username'";
    $resultado = mysqli_query($con, $query);
    $filas = mysqli_num_rows($resultado);
    desconectar($con);
    if ($filas > 0) {
        // Comprobamos que la contraseña es correcta
        $fila = mysqli_fetch_array($resultado);
        extract($fila);
        return password_verify($pass, $password);
    } else {    // Este else no hace falta
        return false;
    }
}

function insertUser($nusuario, $passcif, $nombre, $apellido, $tipo) {
    $conexion = conectar("msg");
    $insert = "insert into user values "
            . "('$nusuario', '$passcif', '$nombre', '$apellido', $tipo)";
    if (mysqli_query($conexion, $insert)) {
        echo "<p>Usuario dado de alta</p>";
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}

function existeUsuario($nombre_usuario) {
    $conexion = conectar("msg");
    $consulta = "select * from user where username='$nombre_usuario';";
    // Ejecutamos la consulta
    $resultado = mysqli_query($conexion, $consulta);
    // Contamos cuantas filas tiene el resultado
    $contador = mysqli_num_rows($resultado);
    desconectar($conexion);
    if ($contador == 0) {
        return false;
    } else {
        return true;
    }
}

function selectAllUsers() {
    $con = conectar('msg');
    $select = "select * from user";
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}

function borrarUser($nombreUser) {
    $con = conectar("msg");
    $delete = "delete from user where username= '$nombreUser';";
    if (mysqli_query($con, $delete)) {
        echo "Usuario Borrado";
    } else {
        echo mysqli_error($con);
    }
    desconectar($con);
}

function insertEvent($username) {
    $conexion = conectar("msg");
    $insert = "insert into event values "
            . "('', '$username', (select current_timestamp), 'I')";
    if (mysqli_query($conexion, $insert)) {
        echo "<p>Usuario dado de alta</p>";
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}

function selectRecentLoginByUsername($nombreUser) {
    $con = conectar('msg');
    $select = "select max(date) as LastLog from event where user = '$nombreUser'";
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}

function selectRankingMessages() {
    $con = conectar('msg');
    $select = "select sender, count(idmessage) as total from message group by sender order by count(idmessage) desc";
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}

function insertEventListMessage($username) {
    $conexion = conectar("msg");
    $insert = "insert into event values "
            . "('', '$username', (select current_timestamp), 'C')";
    if (mysqli_query($conexion, $insert)) {
        
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}

function selectMessagesByUser($username, $posicion, $cantidad) {
    $con = conectar('msg');
    $select = "select * from message where receiver= '$username' order by date desc limit $posicion, $cantidad";
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}

function changeread($idmessage) {
    $con = conectar("msg");
    $update = "update message set message.read =1 where idmessage=$idmessage";
    if (mysqli_query($con, $update)) {
        
    } else {
        echo mysqli_error($con);
    }
    desconectar($con);
}

function selectByIdMessage($idmessage) {
    $con = conectar('msg');
    $select = "select * from message where idmessage= '$idmessage'";
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}

function totalMessages($username) {
    $con = conectar("msg");
    $select = "select count(*) as count from message where receiver = '$username'";
    $resultado = mysqli_query($con, $select);
    $fila = mysqli_fetch_array($resultado);
    extract($fila);
    desconectar($con);
    return $count;
}

function selectMessagesSentByUser($username, $posicion, $cantidad) {
    $con = conectar('msg');
    $select = "select * from message where sender= '$username' order by date desc limit $posicion, $cantidad";
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}

function totalMessagesSent($username) {
    $con = conectar("msg");
    $select = "select count(*) as count from message where sender = '$username'";
    $resultado = mysqli_query($con, $select);
    $fila = mysqli_fetch_array($resultado);
    extract($fila);
    desconectar($con);
    return $count;
}

function selectMessages($posicion, $cantidad) {
    $con = conectar('msg');
    $select = "select * from message order by date desc limit $posicion, $cantidad";
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}

function totalMessagesAdmin() {
    $con = conectar("msg");
    $select = "select count(*) as count from message";
    $resultado = mysqli_query($con, $select);
    $fila = mysqli_fetch_array($resultado);
    extract($fila);
    desconectar($con);
    return $count;
}

function insertEventCreateMessage($username) {
    $conexion = conectar("msg");
    $insert = "insert into event values "
            . "('', '$username', (select current_timestamp), 'R')";
    if (mysqli_query($conexion, $insert)) {
        
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}

function insertMessage($username, $receptor, $asunto, $cuerpo) {
    $conexion = conectar("msg");
    $insert = "insert into message values "
            . "('', '$username', '$receptor', (select current_timestamp), '', '$asunto', '$cuerpo')";
    if (mysqli_query($conexion, $insert)) {
        echo "Mensaje envidado correctamente";
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}
