<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="" method="POST">
            <p>Nombre de usuario: <input type="text" name="nombreuser"></p>
            <p>Password: <input type="password" name="pass"></p>
            <p>Nombre: <input type="text" name="nombre"></p>
            <p>Apellido: <input type="text" name="apellido"></p>
            <input type="submit" value="Registrarse" name="alta">
        </form>
        <?php
        require_once 'bbdduser.php';
        // Si han pulsado el botón registramos el usuario
        if (isset($_POST["alta"])) {
            // Recogemos el nombre de usuario
            $nusuario = $_POST["nombreuser"];
            // Comprobamos si ya existe
            if (existeUsuario($nusuario) == true) {
                echo "<p>Ya existe ese nombre de usuario en la bbdd</p>";
            } else {
                // Recogemos el resto de datos
                $pass = $_POST["pass"];
                $nombre = $_POST["nombre"];
                $apellido = $_POST["apellido"];
                $tipo = 0;
                $passcif = password_hash($pass, PASSWORD_DEFAULT);

                // Registramos el usuario en la bbdd
                insertUser($nusuario, $passcif, $nombre, $apellido, $tipo);
            }
        }
        ?>
        <p><a href="AdminHome.php">Volver</a></p>
    </body>
</html>



