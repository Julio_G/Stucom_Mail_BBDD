<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar perfil</title>
    </head>
    <body>
        <?php
        require_once 'bbdduser.php';
        session_start();

        if (isset($_SESSION["username"])) {
            $username = $_SESSION["username"];
            $passactual = $_SESSION["password"];

            if (isset($_POST["comprobar"])) {
                $pass = $_POST["pass"];
                if ($passactual == $pass) {
                    echo "<p><a href = 'NewPass.php'>Cambiar Contraseña</a></p>";
                    echo "<p><a href = 'UserHome.php'>Pagina Principal</a></p>";
                } else {
                    echo "Contraseña no coinciden con la Actual";
                    echo "<p><a href = ChangePass.php'>Modificar Contraseña</a></p>";
                }
            } else {
                echo "<form action='' method='POST'>";
                echo "<h2>Perfil de $username</h2>";
                echo "<p>Password Actual: <input type='password' name='pass' required></p>";
                echo "<p><input type='submit' name='comprobar' value='Modificar'></p>";
                echo "</form>";
                echo "<p><a href = 'UserHome.php'>Pagina Principal</a></p>";
            }
        } else {
            echo "Usuario no autentificado";
        }
        ?>
    </body>
</html>
